resource "aws_ecs_cluster" "my_cluster" {
  name = "my-cluster"
}
  
resource "aws_lb" "hello" {
  name                       = "hello-lb"
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.my_sg.id]
  subnets                    = ["subnet-0b41b8183c9e17fd7", "subnet-00300cd3bb7b5ff81"] # Specify your subnets
  enable_deletion_protection = false
}

resource "aws_lb_listener" "hello" {
  load_balancer_arn = aws_lb.hello.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.hello_lb_tg.arn
  }
}

resource "aws_lb_target_group" "hello_lb_tg" {
  name        = "hello-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "vpc-0fcbe028313e70f6c"

  health_check {
    interval = 5
    timeout  = 2
  }
}

resource "aws_ecs_cluster" "cluster" {
  name = "cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}


data "aws_iam_policy_document" "ecs_task_execution_assume_role_policy" {
  statement {
    sid = "assume"
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_assume_role_policy.json
  name = "ecs_task_execution_role"
}

data "aws_iam_policy" "AmazonECSTaskExecutionRolePolicy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_policy" {
  policy_arn = data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy.arn
  role = aws_iam_role.ecs_task_execution_role.name
}

resource "aws_ecs_task_definition" "hello" {
  family = "hello-world"
  execution_role_arn = aws_iam_role.ecs_task_execution_role.arn
  container_definitions = jsonencode([
    {
      name      = "hello-world"
      image     = "ajay14/kasim-app"
      essential = true

      portMappings = [
        {
          containerPort = 80
        }
      ]
    }
  ])

  requires_compatibilities = [
    "FARGATE"
  ]

  network_mode = "awsvpc"
  cpu          = "256"
  memory       = "512"
}

resource "aws_ecs_service" "hello" {
  name            = "hello-world"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.hello.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  load_balancer {
    target_group_arn = aws_lb_target_group.hello_lb_tg.arn
    container_name   = "hello-world"
    container_port   = 80
  }

  network_configuration {
    subnets          = ["subnet-0b41b8183c9e17fd7", "subnet-00300cd3bb7b5ff81"] # Specify your subnets
    assign_public_ip = true
    security_groups  = [aws_security_group.my_sg.id]
  }
}

resource "aws_security_group" "my_sg" {
  name        = "my-sg2"
  description = "Allow HTTP inbound traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
