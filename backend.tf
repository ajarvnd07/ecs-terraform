terraform {
  backend "s3" {
    bucket         = "ajay-terraform-statefile"
    key            = "terraform.tfstate"
    region         = "us-east-2"  # Specify your AWS region
    dynamodb_table = "ajay-state-lock"
    encrypt        = true
  }
}